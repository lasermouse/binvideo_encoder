# Bin Video Encoder


## Install

You will need to have installed on your computer:

- Python 3 (version >= 3.6),
- [ffmpeg](https://www.ffmpeg.org/).

Create a virtual environment, using `venv` or `conda`, and install the package in:
```
python3 -m venv binvideo_encoder_venv  # create the virtual environment
source binvideo_encoder_venv/bin/activate  # activate it
pip install git+https://bitbucket.org/riomaxim/binvideo_encoder
```

## Usage

From a terminal, activate the previously create virtual environment:
```
source binvideo_encoder_venv/bin/activate  # activate it
```
and then use the `binvideo_encoder.py` script to convert your videos:
```
binvideo_encoder.py my_cool_video.bin my_smaller_but_still_cool_video.avi <col> <row>
```
where `<col>` and `<row>` are the number of columns and rows for one frame, in
pixels.

Use the flag `-q` to change the quality setting.

Use the flag `-h` flag to get the help message.
