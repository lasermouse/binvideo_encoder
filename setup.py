from setuptools import setup, find_packages

setup(
    name="BinVideoEncoder",
    version="0.1",
    author="Maxime RIO",
    author_email="maxime.rio@ucl.ac.uk",
    description="Convert .bin video generate by Labview to compressed .avi files",
    packages=find_packages(),
    scripts=["binvideo_encoder.py"],
    python_requires=">=3.6",
    install_requires=[
        "defopt>=5.1.0",
        "tqdm>=4.42.1",
        "numpy>=1.18.1",
        "imageio-ffmpeg>=0.3.0",
    ],
)
