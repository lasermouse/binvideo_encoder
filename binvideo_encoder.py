#!/usr/bin/env python3

from pathlib import Path
from typing import Tuple

import defopt
import numpy as np
from tqdm import tqdm
from imageio_ffmpeg import write_frames


def main(
    input_file: Path,
    output_file: Path,
    frame_format: Tuple[int, int],
    *,
    quality: float = 5
):
    """Convert a video from Labview (bin format) to a compressed .avi file

    :param input_file: input .bin file
    :param output_file: output compressed file
    :param frame_format: frame resolution (columns and lines)
    :param quality: measure for quality)
    """
    crf = int((1 - quality / 10.0) * 51)
    print(f"Equivalent crf for libx264 is {crf}.")

    video_arr = np.memmap(
        input_file,
        dtype=[("header", np.uint8, 12), ("frame", np.uint8, frame_format)],
        mode="r",
    )

    writer = write_frames(
        str(output_file), frame_format, pix_fmt_in="gray", quality=quality
    )
    writer.send(None)
    with tqdm(total=len(video_arr)) as pbar:
        for i, frame in enumerate(video_arr["frame"]):
            writer.send(frame)
            pbar.update(1)
    writer.close()


if __name__ == "__main__":
    defopt.run(main)
